// alert('yo')

/*
	Mini activity:
		display the ff. student number in our console.

		2020-1923
		2020-1924
		2020-1925
		2020-1926
		2020-1927
*/

let student1 = '2020-1923'
let student2 = '2020-1924'
let student3 = '2020-1925'
let student4 = '2020-1926'
let student5 = '2020-1927'

console.log(student1)
console.log(student2)
console.log(student3)
console.log(student4)
console.log(student5)


/*
	ARRAYS
		-used to store multiple related values in single variable.
		-declared using square brackets [] also know as 'array literals'
	SYNTAX:
		let/const arrayName = [elemA, elemB.....elemN]
*/


let studentnum=['2020 - 1923','2020 - 1924','2020 - 1925','2020 - 1926','2020 - 1927',]
let grades = [98.5 , 94.3, 89.2, 90.1]
let computerBrands= ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu']

let mixedArr = [12, 'asus', null, undefined, {}]
console.log(mixedArr)

// Each element of an array can also be written in a separate line
let myTasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass']

console.log(myTasks)

// we can also store the values of separate variables in an array
let city1 = 'tokyo'
let city2 = 'Manila'
let city3 = 'Jakarta'

let cities = [city1,city2,city3]
console.log(cities)


// Array Length Property
console.log(myTasks.length) //4
console.log(cities.length) //3


let blankArr = []
console.log(blankArr.length)

let fullName='jamie Noble'
console.log(fullName.length) //11

myTasks.length = myTasks.length -1
console.log(myTasks.length)
console.log(myTasks)


cities.length--
console.log(cities)

// can we do delete a character in a string using .length property?

console.log(fullName.length)
fullName.length = fullName.length -1
console.log(fullName.length)
fullName.length--
console.log(fullName)

let theBeatles = ['John', 'Paul', 'Ringo', 'George']
console.log(theBeatles.length)
theBeatles.length++
console.log(theBeatles.length)
console.log(theBeatles)

//array[i] = 'new value'
theBeatles[4] ='rupert'
console.log(theBeatles)

/*
	Accessing Elements of an array

	syntax :
		arrayName[index]
*/

console.log(grades[0])
console.log(computerBrands[3])

console.log(grades[20])

let lakersLegends = ['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem']
console.log(lakersLegends[1])
console.log(lakersLegends[3])

//can we save array items in another variable?
let currantLaker = lakersLegends[2]
console.log(currantLaker)

console.log('Arrays before Reassignment')
console.log(lakersLegends)
lakersLegends[2]='Pau Gasol'
console.log('Arrays after Reassignment')
console.log(lakersLegends)

//Accessing the last element of an array
let bullsLegend = ['Jordan', 'Pippen','Rodman','Rose','Kukoc']
let lastElementIndex = bullsLegend.length-1
console.log(bullsLegend[lastElementIndex])

//another way
console.log(bullsLegend[bullsLegend.length-1])

// Adding items into the array
let newArr=[]
console.log(newArr)
console.log(newArr[0])
newArr[0]='jennie'
console.log(newArr)
newArr[1]='jisoo'
console.log(newArr)

//What if we want to add an element at the end of our array?
//add lisa in our array

newArr[newArr.length] = "Lisa"
console.log(newArr)

// looping over an Array

for (let index=0; index<newArr.length; index++) {

	console.log(newArr[index])
}

let numArr = [5, 12, 30, 46, 50, 88]

for(let index=0; index<numArr.length;index++) {

	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + ' is divisible by 5')
	} else {
		console.log(numArr[index] + ' is not divisible by 5')
	}
}


//MULTIDIMENTSIONAL ARRAY

let chessBoard =[
['a1', 'b1', 'c1', 'd1', 'e1', 'f1','g1','h1'],
['a2', 'b2', 'c2', 'd2', 'e2', 'f2','g2','h2'],
['a3', 'b3', 'c3', 'd3', 'e3', 'f3','g3','h3'],
['a4', 'b4', 'c4', 'd4', 'e4', 'f4','g4','h4'],
['a5', 'b5', 'c5', 'd5', 'e5', 'f5','g5','h5'],
['a6', 'b6', 'c6', 'd6', 'e6', 'f6','g6','h6'],
['a7', 'b7', 'c7', 'd7', 'e7', 'f7','g7','h7'],
['a8', 'b8', 'c8', 'd8', 'e8', 'f8','g8','h8']]

console.log(chessBoard)
console.log(chessBoard[1][4]) //e2
console.log('Pawn moves to: '+ chessBoard[7][4])